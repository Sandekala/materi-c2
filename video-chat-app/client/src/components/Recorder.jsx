import { useReactMediaRecorder } from 'react-media-recorder';
import { createFileName } from 'use-react-screenshot';

const ScreenRecording = () => {
  const { status, startRecording, stopRecording, mediaBlobUrl } = useReactMediaRecorder({ screen: true });
  const download = (video, { name = 'Client Side Recorder', extension = 'mp4' } = {}) => {
    const a = document.createElement('a');
    a.href = mediaBlobUrl;
    a.download = createFileName(extension, name);
    a.click();
  };

  return (
    <div>
      <button className="btn" onClick={startRecording}>
        Start Recording
      </button>
      <button className="btn" onClick={stopRecording}>
        Stop Recording
      </button>
      <div>
        {mediaBlobUrl && (
          <button className="btn createRoom" onClick={download}>
            Click here to download recorded screen!
          </button>
        )}
      </div>
    </div>
  );
};
export default ScreenRecording;
