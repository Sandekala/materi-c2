import { useEffect, useRef, createRef, useState } from 'react';
import { useScreenshot, createFileName } from 'use-react-screenshot';
import ScreenRecording from './Recorder';

const Room = (props) => {
  const userVideo = useRef();
  const userStream = useRef();
  const partnerVideo = useRef();
  const peerRef = useRef();
  const webSocketRef = useRef();
  const ref = createRef(null);

  const [image, takeScreenShot] = useScreenshot({
    type: 'image/jpeg',
    quality: 1.0,
  });

  const download = (image, { name = 'Client Side Screenshot', extension = 'jpg' } = {}) => {
    const a = document.createElement('a');
    a.href = image;
    a.download = createFileName(extension, name);
    a.click();
  };

  const downloadScreenshot = () => takeScreenShot(ref.current).then(download);

  const openCamera = async () => {
    const allDevices = await navigator.mediaDevices.enumerateDevices();
    const camera = allDevices.filter((device) => device.kind == 'videoinput');

    const constraints = {
      audio: true,
      video: {
        deviceId: camera.deviceId,
      },
    };

    try {
      return await navigator.mediaDevices.getUserMedia(constraints);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    openCamera().then((stream) => {
      userVideo.current.srcObject = stream;
      userStream.current = stream;

      webSocketRef.current = new WebSocket(`ws://127.0.0.1:8000/join?roomID=${props.match.params.roomID}`);
      // const ws = new WebSocket(`ws://127.0.0.1:8000/join?roomID=${roomID}`);
      // ws.addEventListener('open', () => {
      //   ws.send(JSON.stringify({ join: 'true' }));
      // });
      webSocketRef.current.addEventListener('open', () => {
        webSocketRef.current.send(JSON.stringify({ join: true }));
      });
      webSocketRef.current.addEventListener('message', async (e) => {
        const message = JSON.parse(e.data);

        if (message.join) {
          callUser();
        }
        if (message.offer) {
          handleOffer(message.offer);
        }
        if (message.answer) {
          console.log('Receiving Answer');
          peerRef.current.setRemoteDescription(new RTCSessionDescription(message.answer));
        }
        if (message.iceCandidate) {
          console.log('Receiving and Adding ICE Candidate');
          try {
            await peerRef.current.addIceCandidate(message.iceCandidate);
          } catch (error) {
            console.log('Error Receiving ICE Candidate', error);
          }
        }
      });
    });
  }, []);

  const handleOffer = async (offer) => {
    console.log('Received Offer Creating Answer');
    peerRef.current = createPeer();

    await peerRef.current.setRemoteDescription(new RTCSessionDescription(offer));
    userStream.current.getTracks().forEach((track) => {
      peerRef.current.addTrack(track, userStream.current);
    });
    const answer = await peerRef.current.createAnswer();
    await peerRef.current.setLocalDescription(answer);
    webSocketRef.current.send(JSON.stringify({ answer: peerRef.current.localDescription }));
  };

  const callUser = () => {
    console.log('Calling Other User');
    peerRef.current = createPeer();

    userStream.current.getTracks().forEach((track) => {
      peerRef.current.addTrack(track, userStream.current);
    });
  };
  const createPeer = () => {
    console.log('Creating Peer Connection');
    const peer = new RTCPeerConnection({
      iceServers: [{ urls: 'stun:stun.l.google.com:19302' }],
    });
    peer.onnegotiationneeded = handleNegotiationNeeded;
    peer.onicecandidate = handleIceCandidateEvent;
    peer.ontrack = handleTrackEvent;

    return peer;
  };
  const handleNegotiationNeeded = async () => {
    console.log('Creating Offer');
    try {
      const myOffer = await peerRef.current.createOffer();
      await peerRef.current.setLocalDescription(myOffer);

      webSocketRef.current.send(JSON.stringify({ offer: peerRef.current.localDescription }));
    } catch (error) {
      console.log('Negotiation Error', error);
    }
  };
  const handleIceCandidateEvent = (e) => {
    console.log('Found Ice Candidate');
    if (e.candidate) {
      console.log(e.candidate);
      webSocketRef.current.send(JSON.stringify({ iceCandidate: e.candidate }));
    }
  };
  const handleTrackEvent = (e) => {
    console.log('Received Tracks');
    partnerVideo.current.srcObject = e.streams[0];
    console.log(partnerVideo.current);
  };

  return (
    <>
      <div ref={ref}>
        <div className="video">
          <video autoPlay={true} controls={true} ref={userVideo}></video>
          <div className="border"></div>
          <video autoPlay={true} controls={true} ref={partnerVideo}></video>
        </div>
        <div className="center">
          <h3> Click button bellow to take a screenshot !</h3>
          <h1>👇</h1>
          <button className="btn" style={{ marginBottom: '10px' }} onClick={downloadScreenshot}>
            Take screenshot
          </button>
          <h3> Click button bellow to record screen !</h3>
          <h1>👇</h1>
          <ScreenRecording />
        </div>
      </div>
    </>
  );
};
export default Room;
