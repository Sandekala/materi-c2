import React from 'react';
import { useHistory } from 'react-router-dom';

const CreateRoom = (props) => {
  const history = useHistory();
  const create = async (e) => {
    e.preventDefault();
    const res = await fetch('http://127.0.0.1:8000/create');
    const { room_id } = await res.json();

    // history.push(`/room/${room_id}`);
    props.history.push(`/room/${room_id}`);
  };
  return (
    <div className="center">
      <h1> Click button bellow to start Video Chat !</h1>
      <h1>👇</h1>
      <button className="btn createRoom" onClick={create}>
        Create a Room!
      </button>
    </div>
  );
};

export default CreateRoom;
